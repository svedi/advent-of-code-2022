aggregated_calories = Enumerator.new do |y|
  total_calories = 0
  $stdin.each_line(chomp: true) do |line|
    if line.empty?
      y << total_calories
      total_calories = 0
    else
      total_calories += line.to_i
    end
  end
end

puts aggregated_calories.max(3).sum
