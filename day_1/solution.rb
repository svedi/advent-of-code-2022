total_calories = 0
max_calories = [-2,-1,0]

$stdin.each do |line|
  if line == "\n"
    max_calories << total_calories
    max_calories.min
                .then { |min| max_calories.index(min) }
                .then { |index| max_calories.delete_at(index) }
    total_calories = 0
  else
    total_calories += line.to_i
  end
end

puts max_calories.sum
